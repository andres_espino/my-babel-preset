const defaultTargets = "ie 11, chrome 58, > 1%";
const modernTargets =
  "last 2 Chrome versions, last 2 Firefox versions, last 2 Safari versions, last 2 Edge versions";
const targets = () =>
  process.env.BABEL_ENV === "modern" ? modernTargets : defaultTargets;

module.exports = function () {
  return {
    presets: [
      [
        require("@babel/preset-env"),
        {
          targets: targets(),
          useBuiltIns: "usage",
          corejs: 3,
        },
      ],
      require("@babel/preset-react"),
    ],
    plugins: [].concat(
      process.env.BABEL_ENV === "modern"
        ? []
        : [require("@babel/plugin-syntax-dynamic-import")]
    ),
  };
};
